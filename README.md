## Sobre o projeto:
Este projeto consiste na implementação do algoritmo A*. Algoritmo que já data de 1968, tem como objetivo obter  o melhor percurso, ou seja o percurso mais barato, “inclinando” a escolha do trajeto a ser percorrido, por influência de uma heurística e soma de custos do trajeto. Neste projeto também foram incluídos obstáculos (visiveis e invisiveis), o que torna a obtenção do objetivo mais desafiante quando o obstáculo é invisível, porque implica encarar o problema de diferente forma. Deve haver a demonstração do pensamento do agente e todo o planeamento do percurso a executado até ao objetivo final.
A tomada de decisão deve portanto basear a tomada de decisão na função F(n) = G(n) + H(n), com vista a obter o percurso mais barato (solução ótima) em detrimento de outras, evitando os obstáculos encontrados pelo caminho.
## Como instalar
Para correr o servidor e o cliente, o utilizador deve ter instalada a versão 3 do Python. Além do Python 3, o cliente necessita da biblioteca Pillow.

## Como instalar:
Para correr o servidor e o cliente, o utilizador deve ter instalada a versão 3 do Python. Além do Python 3, o cliente necessita da biblioteca Pillow.

## Como correr:
### Para correr o servidor:
```python server/main.py``` 

### Para correr o algoritmo A*:
```python client/A_star.py```

## Como configurar:  
A configuração do ambiente e do agente é feita no ficheiro **config.json**, através da alteração dos valores associados a cada string. 

## Testes
Estão disponíveis diversos ficheiros json com configurações do mapa que foram utilizados como testes.
 - config.json
 - config2.json
 - config3.json

## Contribuidores:
 - Bárbara Galama
 - Diogo Figueiredo